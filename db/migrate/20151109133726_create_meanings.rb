class CreateMeanings < ActiveRecord::Migration
  def change
    create_table :meanings do |t|
      t.integer :name_id
      t.string :meaning
      t.timestamps null: false
    end
  end
end
