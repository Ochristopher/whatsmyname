class Meaning < ActiveRecord::Base
  belongs_to :name
  validates :meaning, presence: true
end
