class Name < ActiveRecord::Base
  has_many :meanings
  accepts_nested_attributes_for :meanings, reject_if: :all_blank, allow_destroy: true

  validates :name, presence: true, uniqueness: {case_sensitive: false}

  def self.search(search)
    where("name ILIKE ?", "%#{search}%")
  end
end
