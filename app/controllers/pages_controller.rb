class PagesController < ApplicationController
  def index
    @q = Name.ransack(params[:q])
    @names = @q.result.page(params[:page]).per(6)
  end
end