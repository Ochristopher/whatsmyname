class NamesController < ApplicationController
  def index
    # @names = Name.all
    @q = Name.ransack(params[:q])
    @names = @q.result.page(params[:page]).per(6)
  end

  def new
    @name = Name.new
    @all_countries = Country.all
  end

  def create
    @all_countries = Country.all
    @name = Name.new(name_params)
    @name.name = params[:name][:name].capitalize
    if @name.save
      flash[:success] = 'You Name had been added'
      redirect_to name_path(@name) 
    else
      flash[:error] = "Sorry something went wrong, please try again"
      render :new
    end
  end

  def show
    @name = Name.find(params[:id])
  end

  private
  def name_params
    params.require(:name).permit(:name, :country, meanings_attributes:[:name_id, :meaning])
  end

end
