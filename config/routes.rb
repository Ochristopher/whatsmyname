Rails.application.routes.draw do
  root 'pages#index'
  resources :names
  resources :meanings
end
